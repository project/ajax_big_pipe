(function ($, Drupal) {

  'use strict';

  function intersectionObserver(placeholderElement) {
    const distance = placeholderElement.getAttribute('data-loading-distance') || '0';

    const observer = new IntersectionObserver((entries) => {
      entries.forEach((entry) => {
        if (entry.isIntersecting) {
          observer.unobserve(entry.target);
          ajaxBigPipeLoad(entry.target);
        }
      });
    }, {
      rootMargin: `${distance}px`
    });

    observer.observe(placeholderElement);
  }

  function ajaxBigPipeProcessDocument(context) {
    let elements = once('ajax-bigpipe', '[data-ajax-placeholder]', context);
    elements.forEach((item) => {
      intersectionObserver(item);
    });
  }

  function ajaxBigPipeLoad(placeholderElement) {
    const ajax = new Drupal.ajax({
      url: drupalSettings.ajaxBigPipe,
      progress: false,
      submit: JSON.parse(placeholderElement.dataset.ajaxPlaceholder)
    })
    ajax.success = function (response, status) {
      // Call all provided AJAX commands.
      Object.keys(response || {}).forEach(function (i) {
        if (response[i].command && ajax.commands[response[i].command]) {
          if (!response[i].selector) {
            if (!response[i].selector) {
              // Set selector by our element.
              response[i].selector = placeholderElement;
            }
          }
          ajax.commands[response[i].command](ajax, response[i], status);
        }
      });
      Drupal.attachBehaviors();

      $('body').trigger('ajaxBigPipeLoad', []);
    };
    ajax.options.method = 'GET';
    ajax.options.type = 'GET';
    ajax.submit.destination = window.location.pathname;
    if(window.location.search) {
      ajax.submit.destination += window.location.search;
    }
    ajax.execute();
  }

  $(window).on('load', function () {
    ajaxBigPipeProcessDocument(document);
  });

})(jQuery, Drupal);