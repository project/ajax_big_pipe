<?php

namespace Drupal\ajax_big_pipe;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Security\TrustedCallbackInterface;

/**
 * Class LazyBlockBuilder
 */
class LazyBlockBuilder implements TrustedCallbackInterface {
  
  /**
   * Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * LazyBlockBuilder constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }
  
  /**
   * Lazy build block.
   *
   * @param string $blockId
   *
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function lazyBlockBuild(string $blockId): array {
    return [
      '#markup'   => $blockId
    ];
  }
  
  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks(): array {
    return ['lazyBlockBuild'];
  }
}