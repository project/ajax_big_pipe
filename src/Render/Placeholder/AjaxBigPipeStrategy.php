<?php

namespace Drupal\ajax_big_pipe\Render\Placeholder;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\Crypt;
use Drupal\Component\Utility\Html;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Render\Markup;
use Drupal\Core\Render\Placeholder\PlaceholderStrategyInterface;
use Drupal\Core\Site\Settings;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides AJAX placeholder strategy.
 *
 * The placeholders on the page will be replaced with AJAX calls.
 */
final class AjaxBigPipeStrategy implements PlaceholderStrategyInterface {
  
  /**
   * The module cookie name for no-JS mark.
   */
  public const NOJS_COOKIE = 'big_pipe_nojs';

  /**
   * {@inheritdoc}
   */
  protected $requestStack;
  
  /**
   * {@inheritdoc}
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public function __construct(RequestStack $request_stack) {
    $this->requestStack = $request_stack;
    $this->renderer = \Drupal::getContainer()->get('renderer');
  }
  
   /**
   * {@inheritdoc}
   */
  public function processPlaceholders(array $placeholders): array {
    
    if ($this->requestStack->getCurrentRequest()->cookies->has(static::NOJS_COOKIE)) {
      return $placeholders;
    }
    
    foreach ($placeholders as $placeholder => $placeholder_render_array) {
      if (!$this->placeholderIsAttributeSafe($placeholder) && !empty($placeholder_render_array['#lazy_builder'][1]['params'])) {
        
        $params = json_decode($placeholder_render_array['#lazy_builder'][1]['params'], TRUE);
        
        if(empty($params['use_ajax_big_pipe'])) {
          continue;
        }
        
        unset($placeholder_render_array['#lazy_builder'][1]['params']);
        
        $callback = $placeholder_render_array['#lazy_builder'][0];
        $args = array_values($placeholder_render_array['#lazy_builder'][1]);
        $token = self::generateToken($callback, $args);

        $placeholders[$placeholder] = [
          '#type' => 'html_tag',
          '#tag' => 'div',
          '#attributes' => [
            'data-ajax-placeholder' => Json::encode([
              'callback' => $callback,
              'args' => $args,
              'token' => $token
            ]),
						'data-loading-distance' => $params['use_preview_height_distance'] ?? NULL,
					],
          '#attached' => [
            'library' => [
              'ajax_big_pipe/ajax'
            ],
            'drupalSettings' => [
              'ajaxBigPipe' => Url::fromRoute('rest.load_ajax_big_pipe.GET')->toString()
            ]
          ]
        ];
        
        if(!empty($params['use_statis_preview'])) {
          $cid = 'ajax_big_pipe_'.$token;
          
          $blockContent = '';
  
          if($cache = \Drupal::cache('data')->get($cid)) {
            $blockContent = $cache->data['blockContent'] ?? '';
            $librarys = $cache->data['librarys'] ?? [];
          }
          else {
            $render_array = [
              '#lazy_builder' => [$callback, $args],
              '#create_placeholder' => FALSE,
            ];
            $blockContent = $this->renderer->renderRoot($render_array);
  
            $this->clearContent($blockContent);
  
            $librarys = $render_array['#attached']['library'] ?? [];
  
            \Drupal::cache('data')->set($cid, [
              'blockContent' => $blockContent,
              'librarys' => $librarys
            ], Cache::PERMANENT);
          }
  
          if(!empty($blockContent)) {
            $placeholders[$placeholder]['content']['#markup'] = Markup::create($blockContent);
          }
          if(!empty($librarys)) {
            foreach ($librarys as $library) {
              $placeholders[$placeholder]['#attached']['library'][$library] = $library;
            }
          }
          $placeholders[$placeholder]['#attached']['library'] = array_values($placeholders[$placeholder]['#attached']['library']);
        }
        else {
          $use_preview_templates = $params['use_preview_templates'] ?? '';
          switch ($use_preview_templates){
						case 'views_template':
							$html = '<div class="ajax-big-pipe-loader-type-views ajax-big-pipe-loader-block">
                          <div class="skeleton-header skeleton-animation"></div>
                          <div class="skeleton-subtitle skeleton-animation"></div>
                          <div class="skeleton-row-container">
                              <div class="skeleton-row skeleton-animation"></div>
                              <div class="skeleton-row skeleton-animation"></div>
                              <div class="skeleton-row skeleton-animation"></div>
                          </div>
                       </div>';
							break;
						case 'block_template':
							$html = '<div class="ajax-big-pipe-loader-type-views ajax-big-pipe-loader-block">
                            <div class="skeleton-header skeleton-animation"></div>
                            <div class="skeleton-paragraph skeleton-animation"></div>
                            <div class="skeleton-paragraph skeleton-animation"></div>
                            <div class="skeleton-paragraph skeleton-animation"></div>
                       </div>';
							break;
						case 'banner_template':
							$html = '<div class="ajax-big-pipe-loader-type-full ajax-big-pipe-loader-block">
                            <div class="skeleton-content-full skeleton-animation"></div>
                        </div>';
							break;
						case 'custom_template':
							if (isset($params['use_preview_templates_custom'])){
								$html = $params['use_preview_templates_custom'];
							}
              else {
								$html = '<span class="ajax-big-pipe-loader"></span>';
							}
							break;
						default:
							$html = '<span class="ajax-big-pipe-loader"></span>';
					}
					$placeholders[$placeholder]['content']['#markup'] = Markup::create($html);        }
      }
    }
    return $placeholders;
  }
  
  /**
   * {@inheritdoc}
   */
  private function placeholderIsAttributeSafe($placeholder): bool {
    return $placeholder[0] !== '<' || $placeholder !== Html::normalize($placeholder);
  }
  
  private function clearContent(&$html) {
    $html_ = Html::load($html);
    
    // Replace links
    foreach ($html_->getElementsByTagName('a') as $a) {
      $a->setAttribute('href', 'javascript:void(0);');
      if($a->hasAttribute('onclick')) {
        $a->removeAttribute('onclick');
      }
    }
    
    // Replace form
    foreach ($html_->getElementsByTagName('form') as $form) {
      $form->parentNode->removeChild($form);
    }
    
    // Replace source
    foreach ($html_->getElementsByTagName('source') as $source) {
      $source->parentNode->removeChild($source);
    }
    
    // Replace images
    foreach ($html_->getElementsByTagName('img') as $img) {
      $img->setAttribute('src', 'data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==');
    }
    
    $html = Html::serialize($html_);
  }
  
  public static function generateToken(string $callback, array $args): string {
    // Use hash salt to protect token against attacks.
    $token_parts = [$callback, $args, Settings::get('hash_salt')];
    return Crypt::hashBase64(serialize($token_parts));
  }
}