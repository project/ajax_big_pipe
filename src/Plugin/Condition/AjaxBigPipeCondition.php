<?php

namespace Drupal\ajax_big_pipe\Plugin\Condition;

use Drupal\Core\Condition\ConditionPluginBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'AJAX BigPipe condition' condition to enable a condition based in module selected status.
 *
 * @Condition(
 *   id = "ajax_big_pipe_condition",
 *   label = @Translation("AJAX BigPipe")
 * )
 *
 */
class AjaxBigPipeCondition extends ConditionPluginBase {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return parent::create($container, $configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {

    $form['use_ajax_big_pipe'] = [
      '#type' => 'checkbox',
      '#title' => t('Use AJAX BigPipe', [], ['context' => 'ajax_big_pipe']),
      '#default_value' => $this->configuration['use_ajax_big_pipe'],
    ];
    $form['use_statis_preview'] = [
      '#type' => 'checkbox',
      '#title' => t('Display static block preview', [], ['context' => 'ajax_big_pipe']),
      '#default_value' => $this->configuration['use_statis_preview'],
      '#states' => [
        'visible' => [
          'input[name="visibility[ajax_big_pipe_condition][use_ajax_big_pipe]"]' => ['checked' => TRUE]
        ]
      ]
    ];
    $form['use_preview_height'] = [
      '#type' => 'number',
      '#title' => t('Fix the height of the block when loading', [], ['context' => 'ajax_big_pipe']),
      '#default_value' => $this->configuration['use_preview_height'],
      '#field_suffix' => 'px',
      '#states' => [
        'visible' => [
          'input[name="visibility[ajax_big_pipe_condition][use_ajax_big_pipe]"]' => ['checked' => TRUE],
          'input[name="visibility[ajax_big_pipe_condition][use_statis_preview]"]' => ['checked' => FALSE],
        ]
      ]
    ];


		$form['use_preview_height_distance'] = [
			'#type' => 'number',
			'#title' => t('The distance with which the block when loading', [], ['context' => 'ajax_big_pipe']),
			'#default_value' => $this->configuration['use_preview_height_distance'],
			'#field_suffix' => 'px',
			'#states' => [
				'visible' => [
					'input[name="visibility[ajax_big_pipe_condition][use_ajax_big_pipe]"]' => ['checked' => TRUE],
					'input[name="visibility[ajax_big_pipe_condition][use_statis_preview]"]' => ['checked' => FALSE],
				]
			]
		];

		$form['use_preview_templates'] = [
			'#type' => 'select',
			'#title' => t('Choose an animation template', [], ['context' => 'ajax_big_pipe']),
			'#default_value' => $this->configuration['use_preview_templates'],
			'#options' => [
        'default' => t('Default', [], ['context' => 'ajax_big_pipe']),
        'views_template' => t('Views template', [], ['context' => 'ajax_big_pipe']),
        'block_template' => t('Block template', [], ['context' => 'ajax_big_pipe']),
        'banner_template' => t('Banner template', [], ['context' => 'ajax_big_pipe']),
        'custom_template' => t('Custom template', [], ['context' => 'ajax_big_pipe']),
      ],
			'#states' => [
				'visible' => [
					'input[name="visibility[ajax_big_pipe_condition][use_ajax_big_pipe]"]' => ['checked' => TRUE],
					'input[name="visibility[ajax_big_pipe_condition][use_statis_preview]"]' => ['checked' => FALSE],
				]
			]
		];

		$form['use_preview_templates_custom'] = [
			'#type' => 'textarea',
			'#title' => t('Add custom markup for loading', [], ['context' => 'ajax_big_pipe']),
			'#default_value' => $this->configuration['use_preview_templates_custom'],
			'#states' => [
				'visible' => [
					'input[name="visibility[ajax_big_pipe_condition][use_ajax_big_pipe]"]' => ['checked' => TRUE],
					'input[name="visibility[ajax_big_pipe_condition][use_statis_preview]"]' => ['checked' => FALSE],
					':input[name="visibility[ajax_big_pipe_condition][use_preview_templates]"]' => ['value' => 'custom_template'],
				]
			]
		];

    $form = parent::buildConfigurationForm($form, $form_state);
    $form['negate']['#access'] = FALSE;
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['use_ajax_big_pipe'] = $form_state->getValue('use_ajax_big_pipe');
    $this->configuration['use_statis_preview'] = $form_state->getValue('use_statis_preview');
    $this->configuration['use_preview_height'] = $form_state->getValue('use_preview_height');
		$this->configuration['use_preview_height_distance'] = $form_state->getValue('use_preview_height_distance');
		$this->configuration['use_preview_templates'] = $form_state->getValue('use_preview_templates');
		$this->configuration['use_preview_templates_custom'] = $form_state->getValue('use_preview_templates_custom');
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'use_ajax_big_pipe' => NULL,
      'use_statis_preview' => NULL,
      'use_preview_height' => 200,
			'use_preview_height_distance' => 100,
			'use_preview_templates' => NULL,
			'use_preview_templates_custom' => NULL,
    ] + parent::defaultConfiguration();
  }

  /**
   * Evaluates the condition and returns TRUE or FALSE accordingly.
   *
   * @return bool
   *   TRUE if the condition has been met, FALSE otherwise.
   */
  public function evaluate() {
    return TRUE;
  }

  /**
   * Provides a human readable summary of the condition's configuration.
   */
  public function summary() {
    $status = (!empty($this->configuration['use_ajax_big_pipe'])) ? t('enabled') : t('disabled');
    return t('Use AJAX BigPipe', [], ['context' => 'ajax_big_pipe']).': '.$status;
  }

}
