<?php

namespace Drupal\ajax_big_pipe\Plugin\rest\resource;

use Drupal\ajax_big_pipe\Render\Placeholder\AjaxBigPipeStrategy;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RemoveCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Url;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a resource to get view modes by entity and bundle.
 *
 * @RestResource(
 *   id = "load_ajax_big_pipe",
 *   label = @Translation("AJAX BigPipe"),
 *   uri_paths = {
 *     "canonical" = "/api/bigpipe"
 *   }
 * )
 */
class LoadAjaxBigPipe extends ResourceBase {

  protected $request;
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->logger = $container->get('logger.factory')->get('ajax_big_pipe');
    $instance->request = $container->get('request_stack');
    $instance->renderer = $container->get('renderer');
    return $instance;
  }
  
  /**
   * {@inheritdoc}
   */
  protected function getBaseRouteRequirements($method) {
    return [
      '_access' => 'TRUE'
    ];
  }
  
  /**
   * Responds to GET requests.
   */
  public function get() {
    
    if ($this->request->getCurrentRequest()->cookies->has(AjaxBigPipeStrategy::NOJS_COOKIE)) {
      return new ModifiedResourceResponse([], 200);
    }
    $response = new AjaxResponse();
    
    $request = $this->request->getCurrentRequest()->query->all();
    
    if(!empty($request['ajax_page_state'])) {
      \Drupal::request()->request->set('ajax_page_state', $request['ajax_page_state']);
    }

    if(!empty($request['destination'])) {
      $destination = parse_url($request['destination']);
      if(!empty($destination['path'])) {
        $result = \Drupal::getContainer()->get('path_alias.manager')->getPathByAlias($destination['path']);

        $path = \Drupal::getContainer()->get('path_processor_manager')->processInbound($result, $this->request->getCurrentRequest());
        if(!empty($path)) {
          $result = $path;
        }

        $url = Url::fromUri("internal:" . $result);
        if($url->isRouted()){
          $params = $url->getRouteParameters();
          if(!empty($params)) {
            foreach ($params  as $key => $param) {
              switch ($key) {
                case 'taxonomy_term':
                case 'filter_entity':
                case 'node':
                case 'user':
                \Drupal::routeMatch()->getParameters()->set($key, \Drupal::entityTypeManager()->getStorage($key)->load($param));
                  break;
                default:
                  \Drupal::routeMatch()->getParameters()->set($key, $param);
                  break;
              }
            }
          }
        }
      }
    }
    
    $callback = $request['callback'] ?? '';
    $args = $request['args'] ?? [];
    $token = $request['token'] ?? '';
    if ($this->validateToken($callback, $args, $token)) {
      $render_array = [
        '#lazy_builder'       => [$callback, $args],
        '#create_placeholder' => FALSE,
      ];
      $html = $this->renderer->renderRoot($render_array);
      
      $response->setAttachments($render_array['#attached']);

      if(!empty($html)) {
        $response->addCommand(new ReplaceCommand(NULL, $html));
      }
      else {
        $response->addCommand(new RemoveCommand(NULL));
      }
    }

    return $response;
  }
  
  private function validateToken(string $callback, array $args, string $provided_token): bool {
    return AjaxBigPipeStrategy::generateToken($callback, $args) == $provided_token;
  }

}
