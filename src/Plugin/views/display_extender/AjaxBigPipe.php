<?php

namespace Drupal\ajax_big_pipe\Plugin\views\display_extender;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\display_extender\DisplayExtenderPluginBase;

/**
 * AJAX BigPipe display extender plugin.
 *
 * @ingroup views_display_extender_plugins
 *
 * @ViewsDisplayExtender(
 *   id = "ajax_big_pipe",
 *   title = @Translation("AJAX BigPipe"),
 *   no_ui = FALSE,
 * )
 */

class AjaxBigPipe extends DisplayExtenderPluginBase {
	
	/**
   * Provide a form to edit options for this plugin.
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    if ($form_state->get('section') == 'ajax_big_pipe') {
			$form['#title'] .= 'AJAX BigPipe';
			$form['is_use'] = [
        '#type'         => 'checkbox',
        '#title'        => $this->t('Status'),
        '#default_value' => $this->isEnabled(),
      ];
		}
  }
	
	/**
   * Handle any special handling on the validate form.
   *
   * Executes on View save button.
   */
  public function submitOptionsForm(&$form, FormStateInterface $form_state) {
    if ($form_state->get('section') == 'ajax_big_pipe') {
      $this->options['enabled'] = $form_state->getValue('is_use');
    }
  }
	
	/**
   * Provide the default summary for options in the views UI.
   *
   * This output is returned as an array.
   */
  public function optionsSummary(&$categories, &$options) {
    $options['ajax_big_pipe'] = [
      'category'    => 'other',
      'title'       => 'AJAX BigPipe',
      'value'       => $this->isEnabled() ? t('Enabled') : t('Disabled'),
    ];
  }
	
	/**
   * Identify this option is enabled or not.
   */
  public function isEnabled() {
    return !empty($this->options['enabled']) ? $this->options['enabled'] : FALSE;
  }
}